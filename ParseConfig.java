import java.util.regex.Matcher;
import java.util.regex.Pattern;

class ParseConfig{

    /**
     * returns : String
     * The below written method takes String pattern and String readFile from which it has to read for input 
     * And provides the data that matched that pattern.
     * Common for all operations which requires REGEX to get data from file.
     * For Example : config.ini
     */

    public static String patternMatcherForConfigFile(String patternRegex, String readFile){
		Pattern pattern = Pattern.compile(patternRegex);
		Matcher matcher = pattern.matcher(readFile);
		String match = "";

        matcher.reset();
		while(matcher.find())
            match = matcher.group(2);

        //Returning the matched regex from patternRegex.
        return match;
    }
}