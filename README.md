This project will automatically create tables in the database along with constraints, provided the database exists. 
User only need to create the JSON file with the given format for creating tables. 
One can use QueryBuilder class for executing database queries.

- DIFFERENT FORMATS FOR CONSTRAINTS :
    * For primary, auto_increment, unique and not null:
        "primary_key" : "PRIMARY KEY",
        "auto_increment" : "AUTO_INCREMENT",
        "unique" : "UNIQUE",
        "not_null" : "NOT NULL"

    * For default:
        "default" : "defaultValue"
 
    * For check:
        "check" : "checkCondition"

    * For foreign key:
        "foreign_key" : "referedTableName(referedColumneName)"


- JSON FORMAT : 
[
    {
        "columns" : [
            {
                "column_name" : "columnName",
                "column_datatype" : "columnDatetype",
                "column_size" : "columnSize",
                "constraints" : {
                    "primary_key" : "PRIMARY KEY",
                    "auto_increment" : "AUTO_INCREMENT"     
                }
            },
            {
                "column_name" : "columnName",
                "column_datatype" : "columnDatetype",
                "column_size" : "columnSize",
                "constraints" : {
                    "unique" : "UNIQUE",
                    "not_null" : "NOT NULL"
                }
            },
            {
                "column_name" : "columnName",
                "column_datatype" : "columnDatetype",
                "column_size" : "columnSize",
                "constraints" : {
                    "check" : "(checkCondition)"
                }
            }
        ]  
    },
    {
        "table" : "tableName",
        "columns" : [
            {
                "column_name" : "columnName",
                "column_datatype" : "columnDatetype",
                "column_size" : "columnSize",
                "constraints" : {
                    "default" : "defaultValue"
                }
            },
            {
                "column_name" : "columnName",
                "column_datatype" : "columnDatetype",
                "column_size" : "columnSize",
                "constraints" : {
                    "default" : "defaultValue"
                }
            },
            {
                "column_name" : "columnName",
                "column_datatype" : "columnDatetype",
                "column_size" : "columnSize",
                "constraints" : {
                    "default" : "defaultValue",
                    "check" : "(checkCondition)"
                }
            }
        ]  
    }
]


- COMMANDS :

1. For Create
    java create table tablename
        It will create a file wiht tablename_current_timestamp in the folder database/migrations only if there does not exists any file with same name(tablename) and with default contents. 

    java create table tablename --force  OR
    java create table tablename -f    
        It will create a file wiht tablename_current_timestamp in the folder database/migrations even if there exists any file with same name(tablename) and with default contents.  

    java create table tablename --empty  OR
    java create table tablename -e   
        It will create a file wiht tablename_current_timestamp in the folder database/migrations only if there does not exists any file with same name(tablename) and without any default contents.    

    Both the flags empty and force can be used together but spaced separated.
    There is also shorthand version for the flags to be used together which is
        * -ef OR -fe

2. For Migration of Tables
    java migrate tables
        It will migrate all the tables into the database.


- QueryBuilder Class :
    QueryBuilder class can be used to run most of your database queries from your application.