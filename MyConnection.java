import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Scanner;
import java.util.Date;
import java.io.File;


public class MyConnection{
    public static Connection getConnection() {
        Connection conn = null;
		try{
            /**
            * Reading the config.ini file.
            */
            
            String configDetails = ReadFiles.readsFromGivenFile("config.ini");
			
            /**
             * Fetching the details required for connecting to the database.
             * hostName
             * username
             * password
             * dbaname
             * port
             */

            String hostName = ParseConfig.patternMatcherForConfigFile("(host = )(.*)", configDetails);
            String username = ParseConfig.patternMatcherForConfigFile("(username = )(.*)", configDetails);
            String password = ParseConfig.patternMatcherForConfigFile("(password = )(.*)", configDetails);
            String dbname = ParseConfig.patternMatcherForConfigFile("(dbname = )(.*)", configDetails);
            String portNo = ParseConfig.patternMatcherForConfigFile("(port = )(.*)", configDetails);

            /**
            * Connecting from database using above fetched details.
            */

            conn = DriverManager.getConnection("jdbc:mysql://" + hostName + ":" + portNo + "/" + dbname, username, password);
            return conn;

        }catch(SQLException sqle){
            System.out.println("Some SQLException Occured : "  + sqle);
        }catch(Exception e){
            System.out.println("Exception Occured : " + e);
        }
        return conn;
    }
    public static void main(String[] args) {
        
        Connection conn = MyConnection.getConnection();

        try {
            if(conn != null) {
                Scanner scanner = new Scanner(System.in);
                String inputCommand = "";
                String tableNameWithFlags = "";
                String tableNameWithoutFlags = "";
                String flags = "";

                /**
                * Infinitely halting the program (Until user types "exit") so that commands can be fetched.
                */

                while(!("exit").equalsIgnoreCase(inputCommand)){
                    inputCommand = scanner.nextLine();
                    inputCommand = inputCommand.toLowerCase();

                    if(("exit").equalsIgnoreCase(inputCommand))
                        System.exit(1);  

                    else if(inputCommand.contains("java create table ")){
                        tableNameWithFlags = inputCommand.substring(18, inputCommand.length());

                        if(tableNameWithFlags.contains(" ")){
                            //Input is with some flag. 

                            tableNameWithoutFlags = tableNameWithFlags.substring(0, tableNameWithFlags.indexOf(" "));
                            flags = tableNameWithFlags.substring(tableNameWithFlags.indexOf(" ")+1, tableNameWithFlags.length());

                            /**
                            * Normal Create (java create table tablename)
                                Will Create a new file with the given tablename if and onlty if file with same tablename does not exists.
                            
                            * Force Create (java create table tablename --force)
                                Will Create a new file with the given tablename even if file with same tablename exists.

                            * Empty Create (java create table tablename --empty)
                                Will Create a new file with the given tablename wihtout template (defaultContents).

                            * Both (java create table tablename --force --empty || java create tabl tablename --empty --force)
                                Will Create a new file with the given tablename even if file with same tablename exists, but the new file will be empty (without template).

                            * Shorthand

                            * For Force (java create table tablename -f)
                            * For Empty (java create table tablename -e)
                            * For Both (java create table tablename -fe || java create table tablename -ef)

                            */ 

                            /**
                                        If this                                      Not this

                            * --force --empty || --empty --force            -f | -e | -ef | -fe
                            * -fe || -ef                                    --force | --empty | -f | -e
                            * -f -e  | -e -f                                --force | --empty | -fe | -ef
                            * --force -e | -e --force                       -f | -fe | -ef | --empty
                            * --empty -f | -f --empty                       -e | -fe | -ef |--force
                                            
                            * --force                                       -f | -fe | -ef | -e | --empty
                            * -f                                            --force | --empty | -fe | -ef | -e 
                            
                            * --empty                                       -f | -fe | -ef | -e | --force
                            * -e                                            --force | --empty | -fe | -ef | -f
                            
                            */


                            if(
                            (
                                (flags.equalsIgnoreCase("--force --empty") || flags.equalsIgnoreCase("--empty --force")) 
                                &&
                                (!(
                                    flags.equalsIgnoreCase("-fe") && flags.equalsIgnoreCase("-ef") && 
                                    flags.equalsIgnoreCase("-f") && flags.equalsIgnoreCase("-e")
                                ))
                            ) ||
                            (
                                (flags.equalsIgnoreCase("-fe") || flags.equalsIgnoreCase("-ef")) && 
                                (!(
                                    flags.equalsIgnoreCase("--empty") && flags.equalsIgnoreCase("--force") && 
                                    flags.equalsIgnoreCase("-e") && flags.equalsIgnoreCase("-f")
                                ))
                            ) ||
                            (
                                (flags.equalsIgnoreCase("-e -f") || flags.equalsIgnoreCase("-f -e")) && 
                                (!(
                                    flags.equalsIgnoreCase("--force") && flags.equalsIgnoreCase("--empty") && 
                                    flags.equalsIgnoreCase("-fe") && flags.equalsIgnoreCase("-ef")
                                ))
                            ) ||
                            (
                                (flags.equalsIgnoreCase("-e --force") || flags.equalsIgnoreCase("--force -e")) && 
                                (!(
                                    flags.equalsIgnoreCase("--empty") &&
                                    flags.equalsIgnoreCase("-fe") && flags.equalsIgnoreCase("-ef") &&
                                    flags.equalsIgnoreCase("-f")
                                ))
                            ) ||
                            (
                                (flags.equalsIgnoreCase("--empty -f") || flags.equalsIgnoreCase("-f --empty")) && 
                                (!(
                                    flags.equalsIgnoreCase("--force") &&
                                    flags.equalsIgnoreCase("-fe") && flags.equalsIgnoreCase("-ef") &&
                                    flags.equalsIgnoreCase("-e")
                                ))
                            ) 
                            ){
                                //Create file even if exists and without template.
                                MyCommands.createCommand(tableNameWithoutFlags, true, true);

                            }else if(
                                (
                                    (flags.equalsIgnoreCase("--force")) && 
                                    (!(
                                        flags.equalsIgnoreCase("--empty") && 
                                        flags.equalsIgnoreCase("-fe") && flags.equalsIgnoreCase("-ef") && 
                                        flags.equalsIgnoreCase("-f") && flags.equalsIgnoreCase("-e")
                                    ))
                                ) ||
                                (
                                    (flags.equalsIgnoreCase("-f")) && 
                                    (!(
                                        flags.equalsIgnoreCase("--force") && flags.equalsIgnoreCase("--empty") && 
                                        flags.equalsIgnoreCase("-fe") && flags.equalsIgnoreCase("-ef") && 
                                        flags.equalsIgnoreCase("-e")
                                    ))
                                )
                            ){
                                //Create file even if exists and with template.
                                MyCommands.createCommand(tableNameWithoutFlags, true, false);

                            }else if(
                                (
                                    (flags.equalsIgnoreCase("--empty")) && 
                                    (!(
                                        flags.equalsIgnoreCase("--force") && 
                                        flags.equalsIgnoreCase("-fe") && flags.equalsIgnoreCase("-ef") && 
                                        flags.equalsIgnoreCase("-f") && flags.equalsIgnoreCase("-e")
                                    ))
                                ) ||
                                (
                                    (flags.equalsIgnoreCase("-e")) && 
                                    (!(
                                        flags.equalsIgnoreCase("--force") && flags.equalsIgnoreCase("--empty") && 
                                        flags.equalsIgnoreCase("-fe") && flags.equalsIgnoreCase("-ef") && 
                                        flags.equalsIgnoreCase("-f")
                                    ))
                                )
                            ){
                                //Create file only if it does not exists but without template.
                                MyCommands.createCommand(tableNameWithoutFlags, false, true);

                            }else
                                System.out.println("Create command with such option does not exists!");

                        }else{
                            //Create file only if it does not exists with template.
                    
                            //Input is wihtout any flags.
                            tableNameWithoutFlags = tableNameWithFlags;
                            
                            //Calling the createCommand which will create the JSON file for specified table along wiht some default contents.

                            MyCommands.createCommand(tableNameWithoutFlags, false, false);
                        }
                    }
                        
                    else if(("java migrate tables").equalsIgnoreCase(inputCommand)){
                        /**
                        * Getting all the jsonFiles from directory to get tableName and its details for parsing. 
                        */

                        File directory = new File("database/migrations/");
                        File[] jsonFiles = directory.listFiles();
                        String tablesDetails = "";
                        String tableName = "";

                        for(File jsonFile : jsonFiles){
                            tableName = jsonFile.getName().substring(jsonFile.getName().lastIndexOf("_")+1, jsonFile.getName().lastIndexOf("."));
                            
                            //Reading each JSON file
                            tablesDetails = ReadFiles.readsFromGivenFile(jsonFile.toString());

                            //Parsing JSON into tables
                            if(ParseJSON.parseJSON(conn, tablesDetails, tableName) == 1);
                                System.out.println("Table '" + tableName + "' Migrated Successfully!");
                        }
                    }else
                        //User typed something wrong!
                        System.out.println("Command Not Found!");
                }
            } else {
                System.out.println("Connection Not Found");
            }
        }catch(Exception e){
            System.out.println("Exception Occured : " + e);
		}finally{
			try{
                conn.close();
			}catch(Exception e){
				System.out.println("Exception Occured In Finally : "  + e);
			}
		}
    }
}