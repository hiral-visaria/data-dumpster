
import java.util.HashMap;
import java.util.Date;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.sql.SQLException;
import MyExceptions.*;

public class QueryBuilder{

    //Will store the tableName for current object
    private String tableName;

    //Will store the columnNames to be displayed if get() is called for current object.
    private ArrayList<String> columnNames = new ArrayList<>();

    //Will store the columnNames to be used for grouping if get() is called for current object.
    private ArrayList<String> groupByColumnNames = new ArrayList<>();

    //Will store the columnNames to be used for ordering if get() is called for current object.
    private ArrayList<String> orderByColumnNames = new ArrayList<>();

    //Will store the order for orderBy clause.
    private String order = "ASC";

    //Will store the sql query for joins;
    private String sql = "";

    //Will be used to store the columnName, operator and value used while there is "where" clause in the query.
    private String columnName;
    private String operator;
    private Object value;

    //Will be used to store the columnName, operator and value used while there is "where" clause in the query, operator set as "=" and value set as null.
    private String whereNullColumnName = null;

    //Will be used to store the columnName, operator and value used while there is "where" clause in the query, operator set as "!=" and value set as null.
    private String whereNotNullColumnName = null;

    //Will be used to store the columnName used while there is "where" clause in the query, operator set as "=" and value from whereInValues.
    private String whereInColumnName = null;
    private ArrayList<Object> whereInValues = new ArrayList<>();

    //Will be used to store the columnName used while there is "where" clause in the query, operator set as "!=" and value from whereNotInValues.
    private String whereNotInColumnName = null;
    private ArrayList<Object> whereNotInValues = new ArrayList<>();

    //Will be used to store the columnName used while there is "where" clause in the query for values between start and end.
    private String whereBetweenColumnName = null;
    private String whereBetweenStartValue = null;
    private String whereBetweenEndValue = null;

    //Will be used to store the columnName used while there is "where" clause in the query for values not between start and end.
    private String whereNotBetweenColumnName = null;
    private String whereNotBetweenStartValue = null;
    private String whereNotBetweenEndValue = null;

    //Will be used to store the columnName, operator and value used while there is an "or" clause in the query after "where" clause.
    private ArrayList<String> orColumnName = new ArrayList<>();
    private ArrayList<String> orOperator = new ArrayList<>();
    private ArrayList<Object> orValue = new ArrayList<>();

    //Will be used to store the columnName, operator and value used while there is an "and" clause in the query after "where" clause.	
    private ArrayList<String> andColumnName = new ArrayList<>();
    private ArrayList<String> andOperator = new ArrayList<>();
    private ArrayList<Object> andValue = new ArrayList<>();

    //Will be used to store the limit in the query.	
    private int limit = -1;

    //Will be used to store the offset in the query.	
    private int offset = -1;

    //Will be used to store the column name and value of increment or decrement, positive values represents increment and negative value represents decrement.
    private String incrementDecrementColumnName = null;
    private int incrementDecrementValue = 1;

    //Will be used to store the column name for retrieving the records as per the boolean value of LATEST and OLDEST.
    private String latestOldestColumnName = null;

    /**
    This will store all the values of where, orWhere & andWhere if exists for the current object.
    This will be passed to PreparedStatement. 
    This is done to avoid SQLInjection. 
    */
    private ArrayList<Object> allValues = new ArrayList<>();

    /**
    * boolean values represting what operation is being performed using the current object.
    * by default they are set to false and in the method they are used, the respective boolean variable is made true.
    */
    private boolean INSERT = false;
    private boolean UPDATE = false;
    private boolean DELETE = false;
    private boolean TRUNCATE = false;
    private boolean DROP = false;
    private boolean AGGREGATE = false;
    private boolean INCREMENT = false;
    private boolean DECREMENT = false; 
    private boolean LATEST = false;
    private boolean OLDEST = false;

    //Common boolean variable for any join operation.
    private boolean JOIN = false;

    
    private String aggregateFunctionName = null;
    private String aggregateColumnName = null;

    //imageIndex will store the index of image attribute in the query as we have used HashMap, the order will not be determined beforehand.
	private int imageIndex = 0;
   
    //imageExists is a boolean variable which will tell whether the certain entry consists of any image attribute.
	private boolean imageExists = false;
    
	//byte[] image will store the image, provided it exists.
    private byte[] image;
    
    //Will store the connection object required for get() to execute the query.
    private static Connection conn;
    
    /**
    returns : void (nothing)
    The below written method takes Connection object.
    It will set the connection object of class to this connection object to execute the queries.
    */

    // public void setConnection(Connection conn){
    {
        try {
            this.conn = MyConnection.getConnection();
        } catch(Exception e){
            System.out.println("Exception Occured : " + e);
        }    
    }

    /**
    returns : QueryBuilder (Object)
    The below written method takes tableName.
    It will return current QueryBuilder object with its tableName set as given in arguments.
    */

    public QueryBuilder table(String tableName){
        this.tableName = tableName;
        return this;
    }

    /**
    returns : QueryBuilder (Object)
    The below written method takes tableName and array of columnNames.
    It will return current QueryBuilder object with its tableName & columnNames (ArrayList<String>) set as given in arguments.
    */

    public QueryBuilder table(String tableName, String[] columnNames){
        this.tableName = tableName;;
        for(int i=0; i<columnNames.length; i++)
            this.columnNames.add(columnNames[i]);

        return this;
    }	

    /**
    returns : QueryBuilder (Object)
    The below written method takes columnName, value and also it sets operator to "=".
    It will return current QueryBuilder object with its columnName, operator & value set as given in arguments.
    */

    public QueryBuilder where(String columnName, Object value){
        this.columnName = columnName;
        this.operator = "=";
        this.value = value;
        return this;
    }

    /**
    returns : QueryBuilder (Object)
    The below written method takes columnName, operator and value.
    It will return current QueryBuilder object with its columnName, operator & value set as given in arguments.
    */

    public QueryBuilder where(String columnName, String operator, Object value){
        this.columnName = columnName;
        this.operator = operator;
        this.value = value;
        return this;
    }

    /**
    returns : QueryBuilder (Object)
    The below written method takes columnName and also it sets operator to "=".
    It will return current QueryBuilder object with its columnName, operator set as given in arguments & value set to NULL.
    */

    public QueryBuilder whereNull(String columnName){
        this.whereNullColumnName = columnName;
        return this;
    }

    /**
    returns : QueryBuilder (Object)
    The below written method takes columnName and also it sets operator to "=".
    It will return current QueryBuilder object with its columnName, operator set as given in arguments & value set to NULL.
    */

    public QueryBuilder whereNotNull(String columnName){
        this.whereNotNullColumnName = columnName;
        return this;
    }

    /**
    returns : QueryBuilder (Object)
    The below written method takes columnName and value.
    It will return current QueryBuilder object with its whereInColumnName & whereInValues set as given in arguments.
    */

    public QueryBuilder whereIn(String whereInColumnName, ArrayList<Object> whereInValues){
        this.whereInColumnName = whereInColumnName;
        for(int i=0; i<whereInValues.size(); i++)
            this.whereInValues.add(whereInValues.get(i));

        return this;
    }

    /**
    returns : QueryBuilder (Object)
    The below written method takes columnName and value.
    It will return current QueryBuilder object with its whereNotInColumnName & whereNotInValues set as given in arguments.
    */

    public QueryBuilder whereNotIn(String whereNotInColumnName, ArrayList<Object> whereNotInValues){
        this.whereNotInColumnName = whereNotInColumnName;
        for(int i=0; i<whereNotInValues.size(); i++)
            this.whereNotInValues.add(whereNotInValues.get(i));
            
        return this;
    }

    /**
    returns : QueryBuilder (Object)
    The below written method takes columnName, start and end value.
    It will return current QueryBuilder object with its whereBetweenColumnName, start value and end value set as given in arguments.
    */

    public QueryBuilder whereBetween(String whereBetweenColumnName, String whereBetweenStartValue, String whereBetweenEndValue){
        this.whereBetweenColumnName = whereBetweenColumnName;
        this.whereBetweenStartValue = whereBetweenStartValue;
        this.whereBetweenEndValue = whereBetweenEndValue;

        return this;
    }

    /**
    returns : QueryBuilder (Object)
    The below written method takes columnName, start and end value.
    It will return current QueryBuilder object with its whereBetweenColumnName, start value and end value set as given in arguments.
    */

    public QueryBuilder whereNotBetween(String whereNotBetweenColumnName, String whereNotBetweenStartValue, String whereNotBetweenEndValue){
        this.whereNotBetweenColumnName = whereNotBetweenColumnName;
        this.whereNotBetweenStartValue = whereNotBetweenStartValue;
        this.whereNotBetweenEndValue = whereNotBetweenEndValue;

        return this;
    }

    /**
    returns : QueryBuilder (Object)
    The below written method takes columnName and value.
    It will return current QueryBuilder object with its orColumnNam, orOperator set to "=" & orValue set as given in arguments.
    */

    public QueryBuilder orWhere(String orColumnName, Object orValue){
        this.orColumnName.add(orColumnName);
        this.orOperator.add("=");
        this.orValue.add(orValue);
        return this;
    }

    /**
    returns : QueryBuilder (Object)
    The below written method takes columnName, operator and value.
    It will return current QueryBuilder object with its orColumnName, orOperator & orValue set as given in arguments.
    */

    public QueryBuilder orWhere(String orColumnName, String orOperator, Object orValue){
        this.orColumnName.add(orColumnName);
        this.orOperator.add(orOperator);
        this.orValue.add(orValue);
        return this;
    }

    /**
    returns : QueryBuilder (Object)
    The below written method takes columnName and value.
    It will return current QueryBuilder object with its andColumnName, andOperator set to "=" & andValue set as given in arguments.
    */

    public QueryBuilder andWhere(String andColumnName, Object andValue){
        this.andColumnName.add(andColumnName);
        this.andOperator.add("=");
        this.andValue.add(andValue);
        return this;
    }
    
    /**
    returns : QueryBuilder (Object)
    The below written method takes columnName, operator and value.
    It will return current QueryBuilder object with its andColumnName, andOperator & andValue set as given in arguments.
    */

    public QueryBuilder andWhere(String andColumnName, String andOperator, Object andValue){
        this.andColumnName.add(andColumnName);
        this.andOperator.add(andOperator);
        this.andValue.add(andValue);
        return this;
    }

    /**
    returns : QueryBuilder (Object)
    The below written method takes groupByColumnNames.
    It will return current QueryBuilder object with its groupByColumnNames set as given in arguments.
    */

    public QueryBuilder groupBy(String[] groupByColumnNames){
        this.groupByColumnNames.clear();
        for(int i=0; i<groupByColumnNames.length; i++)
            this.groupByColumnNames.add(groupByColumnNames[i]);

        return this;
    }

    /**
    returns : QueryBuilder (Object)
    The below written method takes orderByColumnNames.
    It will return current QueryBuilder object with its orderByColumnNames set as given in arguments.
    */

    public QueryBuilder orderBy(String[] orderByColumnNames){
        this.orderByColumnNames.clear();
        for(int i=0; i<orderByColumnNames.length; i++)
            this.orderByColumnNames.add(orderByColumnNames[i]);

        this.order = order;
        return this;
    }

    /**
    returns : QueryBuilder (Object)
    The below written method takes orderByColumnNames and the order.
    It will return current QueryBuilder object with its orderByColumnNames and order set as given in arguments.
    */

    public QueryBuilder orderBy(String[] orderByColumnNames, String order){
        this.orderByColumnNames.clear();
        for(int i=0; i<orderByColumnNames.length; i++)
            this.orderByColumnNames.add(orderByColumnNames[i]);

        this.order = order.toUpperCase();

        return this;
    }

    /**
    returns : QueryBuilder (Object)
    The below written method takes orderByColumnNames and the order.
    It will return current QueryBuilder object with its orderByColumnNames and order will be always set to DESC set as given in arguments.
    */

    public QueryBuilder orderByDesc(String[] orderByColumnNames){
        this.orderByColumnNames.clear();
        for(int i=0; i<orderByColumnNames.length; i++)
            this.orderByColumnNames.add(orderByColumnNames[i]);

        this.order = "DESC";

        return this;
    }


    /**
    returns : QueryBuilder (Object)
    The below written method takes value for limiting the output.
    It will return current QueryBuilder object with its limit set as given in argument.
    */

    public QueryBuilder limit(int limit){
        this.limit = limit;
        return this;
    }

    /**
    returns : QueryBuilder (Object)
    The below written method takes value for setting the offset for the output.
    It will return current QueryBuilder object with its offset set as given in argument.
    */

    public QueryBuilder offset(int offset){
        this.offset = offset;
        return this;
    }

    /**
    returns : ResultSet
    throws: WrongUsageOfMethodException, SyntaxError, ConnectionObjectNotFoundException
    The below written method takes no argument.
    It will return the latest row with the specified columns (if mentioned, else all) from given tableName if the condition provided is satisifed. 
    */

    public ResultSet latest() throws WrongUsageOfMethodException, SyntaxError, ConnectionObjectNotFoundException{
        this.LATEST = true;

        return get();
    }

    /**
    returns : ResultSet
    throws: WrongUsageOfMethodException, SyntaxError, ConnectionObjectNotFoundException
    The below written method takes one argument as columnName to be used for used for condition to get the latest row.
    It will return the latest row with the specified columns (if mentioned, else all) from given tableName if the condition provided is satisifed. 
    */

    public ResultSet latest(String latestColumnName) throws WrongUsageOfMethodException, SyntaxError, ConnectionObjectNotFoundException{
        this.LATEST = true;
        this.latestOldestColumnName = latestColumnName;

        return get();
    }

    /**
    returns : ResultSet
    throws: WrongUsageOfMethodException, SyntaxError, ConnectionObjectNotFoundException
    The below written method takes no argument.
    It will return the oldest row with the specified columns (if mentioned, else all) from given tableName if the condition provided is satisifed. 
    */

    public ResultSet oldest() throws WrongUsageOfMethodException, SyntaxError, ConnectionObjectNotFoundException{
        this.OLDEST = true;

        return get();
    }

    /**
    returns : ResultSet
    throws: WrongUsageOfMethodException, SyntaxError, ConnectionObjectNotFoundException
    The below written method takes one argument as columnName to be used for used for condition to get the oldest row.
    It will return the oldest row with the specified columns (if mentioned, else all) from given tableName if the condition provided is satisifed. 
    */

    public ResultSet oldest(String oldestColumnName) throws WrongUsageOfMethodException, SyntaxError, ConnectionObjectNotFoundException{
        this.OLDEST = true;
        this.latestOldestColumnName = oldestColumnName;

        return get();
    }

    /**
    returns : int
    throws: WrongUsageOfMethodException, SyntaxError, ConnectionObjectNotFoundException
    The below written method takes one argument as column name to be incremented.
    It will return all the number of rows affected after performing updation from given tableName.
    */

    public int increment(String incrementColumnName) throws WrongUsageOfMethodException, SyntaxError, ConnectionObjectNotFoundException {
        this.incrementDecrementColumnName = incrementColumnName;
        this.UPDATE = true;
        this.INCREMENT = true;
        
        return execute();
    }

    /**
    returns : int
    throws: WrongUsageOfMethodException, SyntaxError, ConnectionObjectNotFoundException
    The below written method takes two argument as column name and value to be incremented.
    It will return all the number of rows affected after performing updation from given tableName.
    */

    public int increment(String incrementColumnName, int incrementValue) throws WrongUsageOfMethodException, SyntaxError, ConnectionObjectNotFoundException {
        this.incrementDecrementColumnName = incrementColumnName;
        this.incrementDecrementValue = incrementValue;
        this.UPDATE = true;
        this.INCREMENT = true;
        
        return execute();
    }

    /**
    returns : int
    throws: WrongUsageOfMethodException, SyntaxError, ConnectionObjectNotFoundException
    The below written method takes one argument as column name to be decremented.
    It will return all the number of rows affected after performing updation from given tableName.
    */

    public int decrement(String decrementColumnName) throws WrongUsageOfMethodException, SyntaxError, ConnectionObjectNotFoundException {
        this.incrementDecrementColumnName = decrementColumnName;
        this.UPDATE = true;
        this.DECREMENT = true;
        
        return execute();
    }

    /**
    returns : int
    throws: WrongUsageOfMethodException, SyntaxError, ConnectionObjectNotFoundException
    The below written method takes two argument as column name and value to be decremented.
    It will return all the number of rows affected after performing updation from given tableName.
    */

    public int decrement(String decrementColumnName, int decrementValue) throws WrongUsageOfMethodException, SyntaxError, ConnectionObjectNotFoundException {
        this.incrementDecrementColumnName = decrementColumnName;
        this.incrementDecrementValue = decrementValue;
        this.UPDATE = true;
        this.DECREMENT = true;
        
        return execute();
    }

    /**
    returns : QueryBuilder (Object)
    The below written method takes value of column id and also it sets operator to "=".
    It will return current QueryBuilder object with its id column is set to the given value as given in arguments for where clause.
    */

    public QueryBuilder find(int value){
        this.columnName = "id";
        this.operator = "=";
        this.value = value;
        return this;
    }

    /**
        Equi Join / Inner Join
            SELECT {* | columnName1, columnName2, etc} FROM tablename1 INNER JOIN tablename2 ON tablename1.columnName = tablename2.columnName [WHERE CONDITION];

        * returns : QueryBuilder (Object)
        * This methods takes Hashmap of tableName and the columns to be displayed.
        * This method will create a sql query for innerJoin which will display the given columns (innerJoin) and have condition based on columns tableName (tableName_id).
    */

    public QueryBuilder innerJoin(HashMap<String, ArrayList<String>> innerJoin){
        this.sql = "SELECT ";
        boolean asterisk = true;

        for(String key : innerJoin.keySet()){
            if(innerJoin.get(key).size() != 0){
                for(int i=0; i<innerJoin.get(key).size(); i++){
                    asterisk = false;
                    this.sql += key + "." + innerJoin.get(key).get(i) + ", ";
                }
            }
        }

        if(asterisk)
            this.sql += "*";
        else
            this.sql = this.sql.substring(0, this.sql.length()-2);

        this.sql += " FROM ";

        for(String key : innerJoin.keySet())
            this.sql += "`" + key + "` INNER JOIN ";

        this.sql = this.sql.substring(0, this.sql.length()-12);
        this.sql += " ON ";

        Object[] keys = innerJoin.keySet().toArray();

        this.sql += keys[0] + ".id = "; 

        for(int i=1; i<keys.length; i++)
            this.sql += keys[i] + ".id";
        
        this.JOIN = true;

        return this;
    }

    /**
    * returns : QueryBuilder (Object)
    * throws : UnknownColumnException
    * This methods takes Hashmap of tableName and the columns to be displayed and HahashMap of tableName and the columns used for condition.
    * This method will create a sql query for innerJoin which will display the given columns (innerJoinSelectColumns). and have condtion based on columns passed (innerJoinCondColumns).
    */

    public QueryBuilder innerJoin(HashMap<String, ArrayList<String>> innerJoinSelectColumns, HashMap<String, String> innerJoinCondColumns) throws UnknownColumnException{

        boolean same = false;

        for(String keySelect : innerJoinSelectColumns.keySet()){
            same = false;
            for(String keyCond : innerJoinCondColumns.keySet()){		
                if(keyCond.equalsIgnoreCase(keySelect)){
                    same = true;
                    continue;
                }
            }
            if(same == false)
                break;
        }

        if(same == false)
            throw new UnknownColumnException("Unknown Column Mentioned in the Columns List");
        else{
            this.sql = "SELECT ";
            boolean asterisk = true;

            for(String key : innerJoinSelectColumns.keySet()){
                if(innerJoinSelectColumns.get(key).size() != 0){
                    for(int i=0; i<innerJoinSelectColumns.get(key).size(); i++){
                        asterisk = false;
                        this.sql += key + "." + innerJoinSelectColumns.get(key).get(i) + ", ";
                    }
                }
            }

            if(asterisk)
                this.sql += "*";
            else
                this.sql = this.sql.substring(0, this.sql.length()-2);

            this.sql += " FROM ";

            for(String key : innerJoinSelectColumns.keySet())
                this.sql += "`" + key + "` INNER JOIN ";

            this.sql = this.sql.substring(0, this.sql.length()-12);
            this.sql += " ON ";

            for(String key : innerJoinCondColumns.keySet())
                this.sql += key + "." + innerJoinCondColumns.get(key) + " = ";

            this.sql = this.sql.substring(0, this.sql.length()-3);	
        }
	
        this.JOIN = true;

        return this;
    }

    /**
        Cross Join
            SELECT {* | columnName1, columnName2, etc} FROM tablename1 CROSS JOIN tablename2 [WHERE CONDITION];

            * returns : QueryBuilder (Object)
            * This methods takes Hashmap of tableName and the columns to be displayed.
            * This method will create a sql query for crossJoin which will display the given columns (crossJoin) and have condition based on columns tableName (tableName_id).
    */

    public QueryBuilder crossJoin(HashMap<String, ArrayList<String>> crossJoin){
        this.sql = "SELECT ";
        boolean asterisk = true;

        for(String key : crossJoin.keySet()){
            if(crossJoin.get(key).size() != 0){
                for(int i=0; i<crossJoin.get(key).size(); i++){
                    asterisk = false;
                    this.sql += key + "." + crossJoin.get(key).get(i) + ", ";
                }
            }
        }

        if(asterisk)
            this.sql += "* ";
        else
            this.sql = this.sql.substring(0, this.sql.length()-2);

        this.sql += " FROM ";

        for(String key : crossJoin.keySet())
            this.sql += "`" + key + "` CROSS JOIN ";

        this.sql = this.sql.substring(0, this.sql.length()-12);

        this.JOIN = true;

        return this;
    }

    /**
        Natural Join
            SELECT {* | columnName1, columnName2, etc} FROM tableName1 NATURAL JOIN tableName2 [WHERE CONDITION];

            * returns : QueryBuilder (Object)
            * This methods takes Hashmap of tableName and the columns to be displayed.
            * This method will create a sql query for naturalJoin which will display the given columns (naturalJoin) and have condition based on columns tableName (tableName_id).
    */

    public QueryBuilder naturalJoin(HashMap<String, ArrayList<String>> naturalJoin){
        this.sql = "SELECT ";
        boolean asterisk = true;

        for(String key : naturalJoin.keySet()){
            if(naturalJoin.get(key).size() != 0){
                for(int i=0; i<naturalJoin.get(key).size(); i++){
                    asterisk = false;
                    this.sql += key + "." + naturalJoin.get(key).get(i) + ", ";
                }
            }
        }

        if(asterisk)
            this.sql += "* ";
        else
            this.sql = this.sql.substring(0, this.sql.length()-2);

        this.sql += " FROM ";

        for(String key : naturalJoin.keySet())
            this.sql += "`" + key + "` NATURAL JOIN ";

        this.sql = this.sql.substring(0, this.sql.length()-14);

        this.JOIN = true;

        return this;
    }

    /**
        Full Outer Join
            SELECT {* | columnName1, columnName2, etc} FROM tableName1 FULL OUTER JOIN tableName2 [WHERE CONDITION];

            * returns : QueryBuilder (Object)
            * This methods takes Hashmap of tableName and the columns to be displayed.
            * This method will create a sql query for fullOuterJoin which will display the given columns (fullOuterJoin) and have condition based on columns tableName (tableName_id).			
    */

    public QueryBuilder fullOuterJoin(HashMap<String, ArrayList<String>> fullOuterJoin){
        this.sql = "SELECT ";
        boolean asterisk = true;

        for(String key : fullOuterJoin.keySet()){
            if(fullOuterJoin.get(key).size() != 0){
                for(int i=0; i<fullOuterJoin.get(key).size(); i++){
                    asterisk = false;
                    this.sql += key + "." + fullOuterJoin.get(key).get(i) + ", ";
                }
            }
        }

        if(asterisk)
            this.sql += "*";
        else
            this.sql = this.sql.substring(0, this.sql.length()-2);

        this.sql += " FROM ";

        for(String key : fullOuterJoin.keySet())
            this.sql += "`" + key + "` FULL OUTER JOIN ";

        this.sql = this.sql.substring(0, this.sql.length()-17);
        this.sql += " ON ";

        Object[] keys = fullOuterJoin.keySet().toArray();

        this.sql += keys[0] + ".id = "; 

        for(int i=1; i<keys.length; i++){
            this.sql += keys[i] + ".id";
        }
        
        this.JOIN = true;

        return this;
    }

    /**
    * returns : QueryBuilder (Object)
    * throws : UnknownColumnException
    * This methods takes Hashmap of tableName and the columns to be displayed and HahashMap of tableName and the columns used for condition.
    * This method will create a sql query for fullOuterJoin which will display the given columns (fullOuterJoinSelectColumns). and have condtion based on columns passed (fullOuterJoinCondColumns).
    */

    public QueryBuilder fullOuterJoin(HashMap<String, ArrayList<String>> fullOuterJoinSelectColumns, HashMap<String, String> fullOuterJoinCondColumns) throws UnknownColumnException{
        boolean same = false;
		
        for(String keySelect : fullOuterJoinSelectColumns.keySet()){
            same = false;
            for(String keyCond : fullOuterJoinCondColumns.keySet()){		
                if(keyCond.equalsIgnoreCase(keySelect)){
                    same = true;
                    continue;
                }
            }
            if(same == false)
                break;
        }

        if(same == false)
            throw new UnknownColumnException("Unknown Column Mentioned in the Columns List");
        else{
            this.sql = "SELECT ";
            boolean asterisk = true;

            for(String key : fullOuterJoinSelectColumns.keySet()){
                if(fullOuterJoinSelectColumns.get(key).size() != 0){
                    for(int i=0; i<fullOuterJoinSelectColumns.get(key).size(); i++){
                        asterisk = false;
                        this.sql += key + "." + fullOuterJoinSelectColumns.get(key).get(i) + ", ";
                    }
                }
            }

            if(asterisk)
                this.sql += "*";
            else
                this.sql = this.sql.substring(0, this.sql.length()-2);

            this.sql += " FROM ";

            for(String key : fullOuterJoinSelectColumns.keySet())
                this.sql += "`" + key + "` FULL OUTER JOIN ";

            this.sql = this.sql.substring(0, this.sql.length()-17);
            this.sql += " ON ";

            for(String key : fullOuterJoinCondColumns.keySet())
                this.sql += key + "." + fullOuterJoinCondColumns.get(key) + " = ";

            this.sql = this.sql.substring(0, this.sql.length()-3);

            this.JOIN = true;

            return this;
        }
    }

    /**
        Left Outer Join
            SELECT {* | columnName1, columnName2, etc} FROM tableName1 LEFT OUTER JOIN tableName2 [WHERE CONDITION];

            * returns : QueryBuilder (Object)
            * This methods takes Hashmap of tableName and the columns to be displayed.
            * This method will create a sql query for leftOuterJoion which will display the given columns (leftOuterJoion) and have condition based on columns tableName (tableName_id).	
    */

    public QueryBuilder leftOuterJoin(HashMap<String, ArrayList<String>> leftOuterJoin){
        this.sql = "SELECT ";
        boolean asterisk = true;

        for(String key : leftOuterJoin.keySet()){
            if(leftOuterJoin.get(key).size() != 0){
                for(int i=0; i<leftOuterJoin.get(key).size(); i++){
                    asterisk = false;
                    this.sql += key + "." + leftOuterJoin.get(key).get(i) + ", ";
                }
            }
        }

        if(asterisk)
            this.sql += "*";
        else
            this.sql = this.sql.substring(0, this.sql.length()-2);

        this.sql += " FROM ";

        for(String key : leftOuterJoin.keySet())
            this.sql += "`" + key + "` LEFT OUTER JOIN ";

        this.sql = this.sql.substring(0, this.sql.length()-17);
        this.sql += " ON ";

        Object[] keys = leftOuterJoin.keySet().toArray();

        this.sql += keys[0] + ".id = "; 

        
        for(int i=1; i<keys.length; i++){
            this.sql += keys[i] + ".id";
        }

        this.JOIN = true;

        return this;
    }

    /**
    * returns : QueryBuilder (Object)
    * throws : UnknownColumnException
    * This methods takes Hashmap of tableName and the columns to be displayed and HahashMap of tableName and the columns used for condition.
    * This method will create a sql query for fullOuterJoin which will display the given columns (leftOuterJoinSelectColumns). and have condtion based on columns passed (leftOuterJoinCondColumns).
    */

    public QueryBuilder leftOuterJoin(HashMap<String, ArrayList<String>> leftOuterJoinSelectColumns, HashMap<String, String> leftOuterJoinCondColumns) throws UnknownColumnException{
        boolean same = false;

        for(String keySelect : leftOuterJoinSelectColumns.keySet()){
            same = false;
            for(String keyCond : leftOuterJoinCondColumns.keySet()){		
                if(keyCond.equalsIgnoreCase(keySelect)){
                    same = true;
                    continue;
                }
            }
            if(same == false)
                break;
        }

        if(same == false)
            throw new UnknownColumnException("Unknown Column Mentioned in the Columns List");
        else{
            this.sql = "SELECT ";
            boolean asterisk = true;

            for(String key : leftOuterJoinSelectColumns.keySet()){
                if(leftOuterJoinSelectColumns.get(key).size() != 0){
                    for(int i=0; i<leftOuterJoinSelectColumns.get(key).size(); i++){
                        asterisk = false;
                        this.sql += key + "." + leftOuterJoinSelectColumns.get(key).get(i) + ", ";
                    }
                }
            }

            if(asterisk)
                this.sql += "*";
            else
                this.sql = this.sql.substring(0, this.sql.length()-2);

            this.sql += " FROM ";

            for(String key : leftOuterJoinSelectColumns.keySet())
                this.sql += "`" + key + "` LEFT OUTER JOIN ";

            this.sql = this.sql.substring(0, this.sql.length()-17);
            this.sql += " ON ";

            for(String key : leftOuterJoinCondColumns.keySet())
                this.sql += key + "." + leftOuterJoinCondColumns.get(key) + " = ";

            this.sql = this.sql.substring(0, this.sql.length()-3);

            this.JOIN = true;

            return this;
        }
    }

    /**
        Right Outer Join
            SELECT {* | columnName1, columnName2, etc} FROM tableName1 RIGHT OUTER JOIN tableName2 [WHERE CONDITION];

            * returns : QueryBuilder (Object)
            * This methods takes Hashmap of tableName and the columns to be displayed.
            * This method will create a sql query for rightOuterJoin which will display the given columns (rightOuterJoin) and have condition based on columns tableName (tableName_id).	
    */

    public QueryBuilder rightOuterJoin(HashMap<String, ArrayList<String>> rightOuterJoin){
        this.sql = "SELECT ";
        boolean asterisk = true;

        for(String key : rightOuterJoin.keySet()){
            if(rightOuterJoin.get(key).size() != 0){
                for(int i=0; i<rightOuterJoin.get(key).size(); i++){
                    asterisk = false;
                    this.sql += key + "." + rightOuterJoin.get(key).get(i) + ", ";
                }
            }
        }

        if(asterisk)
            this.sql += "*";
        else
            this.sql = this.sql.substring(0,this. sql.length()-2);

        this.sql += " FROM ";

        for(String key : rightOuterJoin.keySet())
            this.sql += "`" + key + "` RIGHT OUTER JOIN ";

        this.sql = this.sql.substring(0, this.sql.length()-18);
        this.sql += " ON ";

        Object[] keys = rightOuterJoin.keySet().toArray();

        this.sql += keys[0] + ".id = "; 

        
        for(int i=1; i<keys.length; i++){
            this.sql += keys[i] + ".id";
        }
        
        this.JOIN = true;

        return this;
    }

    /**
    * returns : QueryBuilder (Object)
    * throws : UnknownColumnException
    * This methods takes Hashmap of tableName and the columns to be displayed and HahashMap of tableName and the columns used for condition.
    * This method will create a sql query for fullOuterJoin which will display the given columns (rightOuterJoinSelectColumns). and have condtion based on columns passed (rightOuterJoinCondColumns).
    */

    public QueryBuilder rightOuterJoin(HashMap<String, ArrayList<String>> rightOuterJoinSelectColumns, HashMap<String, String> rightOuterJoinCondColumns) throws UnknownColumnException{
        boolean same = false;
		
        for(String keySelect : rightOuterJoinSelectColumns.keySet()){
            same = false;
            for(String keyCond : rightOuterJoinCondColumns.keySet()){		
                if(keyCond.equalsIgnoreCase(keySelect)){
                    same = true;
                    continue;
                }
            }
            if(same == false)
               break;
        }

        if(same == false)
            throw new UnknownColumnException("Unknown Column Mentioned in the Columns List");
        else{
            this.sql = "SELECT ";
            boolean asterisk = true;

            for(String key : rightOuterJoinSelectColumns.keySet()){
                if(rightOuterJoinSelectColumns.get(key).size() != 0){
                    for(int i=0; i<rightOuterJoinSelectColumns.get(key).size(); i++){
                        asterisk = false;
                        this.sql += key + "." + rightOuterJoinSelectColumns.get(key).get(i) + ", ";
                    }
                }
            }

            if(asterisk)
                this.sql += "*";
            else
                this.sql = this.sql.substring(0, this.sql.length()-2);

            this.sql += " FROM ";

            for(String key : rightOuterJoinSelectColumns.keySet())
                this.sql += "`" + key + "` RIGHT OUTER JOIN ";

            this.sql = this.sql.substring(0, this.sql.length()-18);
            this.sql += " ON ";
			
            for(String key : rightOuterJoinCondColumns.keySet())
                this.sql += key + "." + rightOuterJoinCondColumns.get(key) + " = ";

            this.sql = this.sql.substring(0, this.sql.length()-3);

            this.JOIN = true;

            return this;
        }
    }

    public ResultSet count() throws WrongUsageOfMethodException, SyntaxError, ConnectionObjectNotFoundException{
        this.AGGREGATE = true;
        this.aggregateFunctionName = "count";
        
        return this.get();
    }

    public ResultSet count(String columnName) throws WrongUsageOfMethodException, SyntaxError, ConnectionObjectNotFoundException{
        this.AGGREGATE = true;
        this.aggregateFunctionName = "count";
        this.aggregateColumnName = columnName;

        return this.get();
    }

    public ResultSet max(String columnName) throws WrongUsageOfMethodException, SyntaxError, ConnectionObjectNotFoundException{
        this.AGGREGATE = true;
        this.aggregateFunctionName = "max";
        this.aggregateColumnName = columnName;

        return this.get();
    }

    public ResultSet min(String columnName) throws WrongUsageOfMethodException, SyntaxError, ConnectionObjectNotFoundException{
        this.AGGREGATE = true;
        this.aggregateFunctionName = "min";
        this.aggregateColumnName = columnName;

        return this.get();
    }

    public ResultSet avg(String columnName) throws WrongUsageOfMethodException, SyntaxError, ConnectionObjectNotFoundException{
        this.AGGREGATE = true;
        this.aggregateFunctionName = "avg";
        this.aggregateColumnName = columnName;

        return this.get();
    }

    /**
    returns : ResultSet
    throws: WrongUsageOfMethodException, SyntaxError, ConnectionObjectNotFoundException
    The below written method takes no argument.
    It will return all the rows with the specified columns (if mentioned, else all) from given tableName if the condition provided satisifed. 
    If no condition is provided, it will return all the rows from given tableName (specified columns or *).
    */

    public ResultSet get() throws WrongUsageOfMethodException, SyntaxError, ConnectionObjectNotFoundException{
        ResultSet rs = null;
        if(this.INSERT != true && this.UPDATE != true && this.DELETE != true){
            if(conn != null){
                if("".equals(sql)){
                    this.sql = "SELECT ";
                    if (this.AGGREGATE == true) {
                        if(this.aggregateFunctionName == "count") {
                            if(this.aggregateColumnName != null) 
                                this.sql += this.aggregateFunctionName + "(`" + this.aggregateColumnName + "`) ";
                            else 
                                this.sql += this.aggregateFunctionName + "(*) ";
                        } 
                        else
                            this.sql += this.aggregateFunctionName + "(`" + this.aggregateColumnName + "`) ";
                    }
                    else if(columnNames.size() == 0)
                        this.sql += "*";
                    else{
                        for(int i=0; i<this.columnNames.size(); i++)
                            this.sql += "`" + this.columnNames.get(i) + "`, ";

                        this.sql = this.sql.substring(0, this.sql.length()-2);
                    }
                    
                    this.sql += " FROM `" + this.tableName + "`";
                }
                
				if(((this.columnName == null && this.operator == null && this.value == null) && this.whereNullColumnName == null && this.whereNotNullColumnName == null) && ((this.orColumnName.size() != 0 && this.orOperator.size() != 0 || this.orValue.size() != 0) || (this.andColumnName.size() != 0 || this.andOperator.size() != 0 || this.andValue.size() != 0)))
                    throw new SyntaxError("Syntax Error Somewhere Around Where Condition");
                else{ 
                    if((this.columnName != null && this.operator != null && this.value != null)){
                        if(this.sql.contains("WHERE") == false){
                            this.sql += " WHERE `" + this.columnName + "` " + this.operator + " ?";
                            this.allValues.add(this.value);
                        }else
                            throw new SyntaxError("Syntax Error Somewhere Around Where Condition");
                    }

                    if((this.whereNullColumnName != null)){
                        if(this.sql.contains("WHERE") == false){
                            this.sql += " WHERE `" + this.whereNullColumnName + "` = null";
                        }else
                            throw new SyntaxError("Syntax Error Somewhere Around Where Condition");
                    }

                    if((this.whereNotNullColumnName != null)){
                        if(this.sql.contains("WHERE") == false){
                            this.sql += " WHERE " + this.whereNotNullColumnName + "` != null";
                        }else
                            throw new SyntaxError("Syntax Error Somewhere Around Where Condition");
                    }

                    if((this.whereInColumnName != null)){
                        if(this.sql.contains("WHERE") == false){
                            this.sql += " WHERE `" + this.whereInColumnName + "` IN (";
                            for(int i=0; i<whereInValues.size(); i++)
                                this.sql += this.whereInValues.get(i) + ", ";

                            this.sql = this.sql.substring(0, this.sql.length()-2);
                            this.sql += ")";
                        }else
                            throw new SyntaxError("Syntax Error Somewhere Around Where Condition");
                    }

                    if((this.whereNotInColumnName != null)){
                        if(this.sql.contains("WHERE") == false){
                            this.sql += " WHERE `" + this.whereNotInColumnName + "` NOT IN (";
                            for(int i=0; i<whereNotInValues.size(); i++)
                                this.sql += this.whereNotInValues.get(i) + ", ";
                            
                            this.sql = this.sql.substring(0, this.sql.length()-2);
                            this.sql += ")";
                        }else
                            throw new SyntaxError("Syntax Error Somewhere Around Where Condition");
                    }

                    if((this.whereBetweenColumnName != null)){
                        if(this.sql.contains("WHERE") == false){
                            this.sql += " WHERE `" + this.whereBetweenColumnName + "` BETWEEN ";
                            this.sql += this.whereBetweenStartValue + " AND ";
                            this.sql += this.whereBetweenEndValue;
                        }else
                            throw new SyntaxError("Syntax Error Somewhere Around Where Condition");
                    }

                    if((this.whereNotBetweenColumnName != null)){
                        if(this.sql.contains("WHERE") == false){
                            this.sql += " WHERE `" + this.whereNotBetweenColumnName + "` NOT BETWEEN ";
                            this.sql += this.whereNotBetweenStartValue + " AND ";
                            this.sql += this.whereNotBetweenEndValue;
                        }else
                            throw new SyntaxError("Syntax Error Somewhere Around Where Condition");
                    }
					
                    if(orColumnName.size() != 0 && orOperator.size() != 0 && orValue.size() != 0){
                        for(int i=0; i<orColumnName.size(); i++){
                            this.sql += " OR `" + this.orColumnName.get(i) + "` " + this.orOperator.get(i) + " ?";
                            this.allValues.add(this.orValue.get(i));
                        }
                    }

                    if(andColumnName.size() != 0 && andOperator.size() != 0 && andValue.size() != 0){
                        for(int i=0; i<andColumnName.size(); i++){
                            this.sql += " AND `" + this.andColumnName.get(i) + "` " + this.andOperator.get(i) + " ?";
                            this.allValues.add(this.andValue.get(i));
                        }
                    }

                    if(groupByColumnNames.size() != 0) {
                        this.sql += " GROUP BY ";
                        for(int i=0; i<this.groupByColumnNames.size(); i++)
                            this.sql += "`" + this.groupByColumnNames.get(i) + "`, ";

                        this.sql = this.sql.substring(0, this.sql.length()-2);
                    }

                    if((this.LATEST == true || this.OLDEST == true) && this.orderByColumnNames.size() != 0 && this.limit != -1) 
                        throw new SyntaxError("Wrong Usage of Methods");
                     
                    if(this.LATEST == true || this.OLDEST == true) {
                        this.sql += " ORDER BY ";

                        if(this.latestOldestColumnName != null) 
                            this.sql += "`" + this.latestOldestColumnName + "`";
                        else 
                            this.sql += "`created_at`";
                        
                        if(this.LATEST == true) 
                            this.sql += " DESC ";
                        else if(this.OLDEST == true)
                            this.sql += " ASC ";
                        
                        this.sql += "LIMIT 1";
                    }

                    if(this.orderByColumnNames.size() != 0) {
                        this.sql += " ORDER BY ";
                        for(int i=0; i<this.orderByColumnNames.size(); i++)
                            this.sql += "`" + this.orderByColumnNames.get(i) + "`, ";

                        this.sql = this.sql.substring(0, this.sql.length()-2);
                        
                        this.sql += " " + this.order;
                    }

                    if(this.limit != -1) {
                        this.sql += " LIMIT " + this.limit; 
                        
                        if(this.offset != -1) {
                            this.sql += " OFFSET " + this.offset; 
                        }
                    } else if(this.offset != -1) {
                        throw new SyntaxError("Unrecognized keyword OFFSET");
                    }
                    
                    this.sql += ";";
                    System.out.println(this.sql);
                    PreparedStatement ps = null;
                    
                    try{
                        int index = 1;
                        ps = conn.prepareStatement(this.sql);
                        
                        for(int i=0; i<allValues.size(); i++){
                            if(this.allValues.get(i) instanceof String || this.allValues.get(i) instanceof Date)
                                ps.setString(index++, allValues.get(i).toString());
                            else if(this.allValues.get(i) instanceof Integer)
                                ps.setInt(index++, Integer.parseInt(allValues.get(i).toString()));
                            else if(this.allValues.get(i) instanceof Double)
                                ps.setDouble(index++, Double.parseDouble(allValues.get(i).toString()));
                            else if(this.allValues.get(i) instanceof Float)
                                ps.setFloat(index++, Float.parseFloat(allValues.get(i).toString()));
                            else if(this.allValues.get(i) instanceof Long)
                                ps.setLong(index++, Long.parseLong(allValues.get(i).toString()));
                            
                        }
                        rs = ps.executeQuery();  
                    }catch(Exception e){
                        System.out.println("Some Exception Occurred While Fetching the Records : "  + e);
                    }    
                }
            }else
                throw new ConnectionObjectNotFoundException("Connection Object Not Found!");
        } else
            throw new WrongUsageOfMethodException("get() method cannot be used for INSERT or UPDATE or DELETE!");

        return rs;
    }

    /**
    returns : ResultSet
    throws: WrongUsageOfMethodException, SyntaxError, ConnectionObjectNotFoundException
    The below written method takes no argument.
    It will return only the first row with the specified columns (if mentioned, else all) from given tableName if the condition provided is satisifed. 
    */

    public QueryBuilder first() throws WrongUsageOfMethodException, SyntaxError, ConnectionObjectNotFoundException{
        if(this.limit != -1) {
            throw new WrongUsageOfMethodException("first() method cannot be used with other used methods!");
        }

        this.limit = 1;
        return this;
    }

    /**
    returns : QueryBuilder
    throws: WrongUsageOfMethodException, SyntaxError, ConnectionObjectNotFoundException
    The below written method takes one argument as columnName.
    It will return only the records with columnName as specified in the agrument. 
    */

    public QueryBuilder pluck(String columnName) throws WrongUsageOfMethodException, SyntaxError, ConnectionObjectNotFoundException{
        // this.columnNames.clear();
        this.columnNames.add(columnName);

        return this;
    }

    /**
    returns : QueryBuilder (object)
    The below written method takes HashMap of String, Object which has key as column_name and value as the insertion value.
    It will set the sql accordingly and set the insertValues for this object.
    */

    public QueryBuilder insert(HashMap<String, Object> insertColumns, byte[] image){
        this.sql = "INSERT INTO `" + tableName +"` (";
        String values = " VALUES (";
        int index = 0;
        for(String key : insertColumns.keySet()){
            index++;
            this.sql += "`" + key + "`, ";
            values += "?, ";
            
            if("image".equals(insertColumns.get(key))){
            	this.imageIndex = index;
	            
	            if(image == null)
	            	this.image = null;
	            else
	                this.image =  Arrays.copyOf(image, image.length);
	            
	            this.imageExists = true;
            }else
            	this.allValues.add(insertColumns.get(key)); 
        }
        
        this.sql = this.sql.substring(0, this.sql.length()-2) + ")";
        values = values.substring(0, values.length()-2) + ")";
        this.sql = this.sql + values;
        this.INSERT = true;

        return this;
    }

    /**
    returns : QueryBuilder (object)
    The below written method takes HashMap of String, Object which has key as column_name and value as the updation value.
    It will set the sql accordingly and set the updateValues for this object.
    */

    public QueryBuilder update(HashMap<String, Object> updateColumns, byte[] image){
        this.sql = "UPDATE `" + this.tableName + "` SET ";
        int index = 0;
        for(String key : updateColumns.keySet()){
            index++;
            this.sql += "`" + key + "` = ?, ";
            this.allValues.add(updateColumns.get(key));
            
            if("image".equals(updateColumns.get(key))){
            	this.imageIndex = index;
	            
	            if(image == null)
	            	this.image = null;
	            else
	                this.image =  Arrays.copyOf(image, image.length);
	            
	            this.imageExists = true;
            }else
            	this.allValues.add(updateColumns.get(key));
        }
   
        sql = sql.substring(0, sql.length()-2);

        this.UPDATE = true;

        return this;
    }

    /**
    returns : QueryBuilder (object)
    The below written method takes no argument.
    It will set the sql accordingly for this object.
    */

    public QueryBuilder delete(){
        this.sql = "DELETE FROM `" + this.tableName + "`";
    	
        this.DELETE = true;
        
        return this;
    }

    /**
    returns : QueryBuilder (object)
    The below written method takes no argument.
    It will set the sql accordingly for this object.
    */

    public QueryBuilder truncate(){
        this.sql = "TRUNCATE TABLE `" + this.tableName + "`";
    	
        this.TRUNCATE = true;
        
        return this;
    }

    /**
    returns : QueryBuilder (object)
    The below written method takes no argument.
    It will set the sql accordingly for this object.
    */

    public QueryBuilder drop(){
        this.sql = "DROP TABLE `" + this.tableName + "`";
    	
        this.DROP = true;
        
        return this;
    }

    /**
    returns : int
    throws: WrongUsageOfMethodException, SyntaxError, ConnectionObjectNotFoundException
    The below written method takes no argument.
    It will return all the number of rows affected after performing insertion, updation or deletion from given tableName.
    */

    public int execute() throws WrongUsageOfMethodException, SyntaxError, ConnectionObjectNotFoundException{
        int rowsAffected = 0;
        if(this.JOIN == false && (this.INSERT == true || this.UPDATE == true || this.DELETE == true)){
            if(conn != null){
                if(this.UPDATE == true || this.DELETE == true){
                    if((this.columnName == null && this.operator == null && this.value == null) && ((this.orColumnName.size() != 0 && this.orOperator.size() != 0 || this.orValue.size() != 0) || (this.andColumnName.size() != 0 || this.andOperator.size() != 0 || this.andValue.size() != 0)))
                        throw new SyntaxError("Syntax Error Somewhere Around Where Condition");
                    else{ 
                        if(this.incrementDecrementColumnName != null) {
                            this.sql = "UPDATE `" + this.tableName + "` SET ";
                            this.sql += "`" + this.incrementDecrementColumnName + "` = " + this.incrementDecrementColumnName;
                            
                            if(this.INCREMENT == true) 
                                this.sql += " + ";
                            else if(this.DECREMENT == true) 
                                this.sql += " - ";
                            
                            this.sql += this.incrementDecrementValue;
                        }

                        if((this.columnName != null && this.operator != null && this.value != null)){
                            if(this.sql.contains("WHERE") == false){
                                this.sql += " WHERE `" + this.columnName + "` " + this.operator + " ?";
                                this.allValues.add(this.value);
                            }else
                                throw new SyntaxError("Syntax Error Somewhere Around Where Condition");
                        }


                        if(orColumnName.size() != 0 && orOperator.size() != 0 && orValue.size() != 0){
                            for(int i=0; i<orColumnName.size(); i++){
                                this.sql += " OR `" + this.orColumnName.get(i) + "` " + this.orOperator.get(i) + " ?";
                                this.allValues.add(this.orValue.get(i));
                            }
                        }
						
                        if(andColumnName.size() != 0 && andOperator.size() != 0 && andValue.size() != 0){
                            for(int i=0; i<andColumnName.size(); i++){
                                this.sql += " AND `" + this.andColumnName.get(i) + "` " + this.andOperator.get(i) + " ?";
                                this.allValues.add(this.andValue.get(i));
                            }
                        }
                    }
                } else if(this.INSERT == true || this.TRUNCATE == true || this.DROP == true){
                    if((this.columnName != null && this.operator != null && this.value != null) || ((this.orColumnName.size() != 0 && this.orOperator.size() != 0 || this.orValue.size() != 0) || (this.andColumnName.size() != 0 || this.andOperator.size() != 0 || this.andValue.size() != 0)))
                        throw new SyntaxError("Syntax Error Somewhere Around Where Condition");
                }

                this.sql += ";";

                PreparedStatement ps = null;
                System.out.println(sql);
                try{
                    int index = 1;
                    ps = conn.prepareStatement(this.sql);
                   
                    for(int i=0; i<allValues.size(); i++){
                        if(index == imageIndex && imageIndex != 0 && imageExists == true)
                            ps.setBytes(index++, this.image);

                        if(this.allValues.get(i) instanceof String || this.allValues.get(i) instanceof Date)
                            ps.setString(index++, this.allValues.get(i).toString());
                        else if(this.allValues.get(i) instanceof Integer)
                            ps.setInt(index++, Integer.parseInt(this.allValues.get(i).toString()));
                        else if(this.allValues.get(i) instanceof Double)
                            ps.setDouble(index++, Double.parseDouble(this.allValues.get(i).toString()));
                        else if(this.allValues.get(i) instanceof Float)
                            ps.setFloat(index++, Float.parseFloat(this.allValues.get(i).toString()));
                        else if(this.allValues.get(i) instanceof Long)
                            ps.setLong(index++, Long.parseLong(this.allValues.get(i).toString()));
                        
                    }

                    rowsAffected = ps.executeUpdate();  

                }catch(Exception e){
                    System.out.println("Some Exception Occurred While Executing the Query : "  + e);
                }
            }else
                throw new ConnectionObjectNotFoundException("Connection Object Not Found!");
        }else
            throw new WrongUsageOfMethodException("execute() method cannot be used for Fetching Records (SELECT Statement) and JOIN Statements!");

        return rowsAffected;
    }
}