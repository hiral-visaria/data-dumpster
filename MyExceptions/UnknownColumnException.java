/**
* It is user defined exception which will be thrown if the columns passed for performing joins are not present in the table.
*/

package MyExceptions;

public class UnknownColumnException extends Exception{
	public UnknownColumnException(String msg){
		super(msg);
	}
}