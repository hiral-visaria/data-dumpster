/**
* It is user defined exception which will be thrown if there is any syntax error near "where" clause (i.e. if "orWhere | andWhere" is used without "where").
*/

package MyExceptions;

public class SyntaxError extends Exception{
	public SyntaxError(String msg){
		super(msg);
	}
}