/**
* It is user defined exception which will be thrown if the connection object is not set before calling the get().
*/

package MyExceptions;

public class ConnectionObjectNotFoundException extends Exception{
	public ConnectionObjectNotFoundException(String msg){
		super(msg);
	}
}