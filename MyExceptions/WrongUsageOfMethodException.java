/**
* It is user defined exception which will be thrown if the get() or execute() is called with wrong query.
* For Example: 
	insert() used with get().
	update() used with get().
	delete() used with get().

	"select" or joins used with execute().
*/

package MyExceptions;

public class WrongUsageOfMethodException extends Exception{
	public WrongUsageOfMethodException(String msg){
		super(msg);
	}
}

