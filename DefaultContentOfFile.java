import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class DefaultContentOfFile{
	/**
	* return : void
	* This method writes the default contents to the JSON file of given tableName.
	*/

	public static void writeDefaultContentInJSONFile(String filePath, String tableName, boolean isEmpty){
		File inFile;

		if(!isEmpty)
			inFile = new File("defaultContents.json");
		else
			inFile = new File("blankContent.json");

		File outFile = new File(filePath);
		
		FileInputStream ins = null;
		FileOutputStream outs = null;
		try
		{
			ins = new FileInputStream(inFile);
			outs = new FileOutputStream(outFile);
			int b;
			while((b = ins.read()) != -1)
				outs.write(b);

		}catch(FileNotFoundException fnfe){
            System.out.println("Some FileNotFoundException Occured : "  + fnfe);
        }catch(IOException ioe){
            System.out.println("Some IOException Occured : "  + ioe);
        }catch(Exception e){
            System.out.println("Exception Occured : " + e);
        }finally{
            try{
				ins.close();
				outs.close();
			}catch(Exception e){
				System.out.println("Exception Occured In Finally : "  + e);
			}
        }
	}
}