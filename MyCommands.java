import java.sql.Timestamp;
import java.util.Date;
import java.io.IOException;
import java.io.File;

class MyCommands{
	public static void createFile(String tableName, boolean isEmpty){
		File jsonFile = new File("database/migrations/" + (new Date().getTime()) + "_" + tableName + ".json");
        try{	
        	jsonFile.createNewFile();
        	System.out.println("File for table " + tableName + " created successfully!");
        	if(!isEmpty)
        		//With Default Contents
        		DefaultContentOfFile.writeDefaultContentInJSONFile(jsonFile.toString(), tableName, false);
        	else
        		//Without Default Contents
        		DefaultContentOfFile.writeDefaultContentInJSONFile(jsonFile.toString(), tableName, true);
        }catch(IOException ioe){
        	System.out.println("Some IOException occured while creating new file : " + ioe);
        }	
	}

	public static void createCommand(String tableName, boolean forced, boolean empty){
		if(!forced){
			File directory = new File("database/migrations/");
	        File[] jsonFiles = directory.listFiles();

	        int fileAlreadyExists = 0;
	        if(jsonFiles.length != 0){
		        String existingFileName = "";
		        for(File json : jsonFiles){
		        	existingFileName = json.getName();
		        	existingFileName = existingFileName.substring(0, existingFileName.lastIndexOf("_"));
		        	if((existingFileName).equalsIgnoreCase(tableName)){
		        		fileAlreadyExists = 1;
		        		break;
		        	}
		        }
		    }
		    
		    if(fileAlreadyExists == 0)
		    	MyCommands.createFile(tableName, empty);
	    	else
	    		System.out.println("File of tablename " + tableName + " already exist! Please try again!");
	    }else
	    	MyCommands.createFile(tableName, empty);
	}
}