import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.PreparedStatement;

class ParseJSON{
    public static int parseJSON(Connection conn, String tablesDetails, String tableName){

        PreparedStatement ps = null;

        /**
        * Regex for fetching all columns from tablesDetails.
        */
        
        String columns = "(\"columns\"([\\s]*):([\\s]*))([\\s\\S]*?)(?=])";
        Pattern columnsPattern = Pattern.compile(columns);
        Matcher columnsMatcher = columnsPattern.matcher(tablesDetails);
        String columnsMatch = "";

        columnsMatcher.reset();

        /**
        * Regex for fetching columnName.
        * Using only columns Regex output.
        */
        
        String columnNameRegex = "(\"column_name\"([\\s]*):([\\s]*)\")(.*?)(\")";
        Pattern columnNamePattern = Pattern.compile(columnNameRegex);
        Matcher columnNameMatcher;
        String columnNameMatch = "";

        /**
        * Regex for fetching columnDatatype.
        * Using only columns Regex output.
        */

        String columnDatatypeRegex = "(\"column_datatype\"([\\s]*):([\\s]*)\")(.*?)(\")";
        Pattern columnDatatypePattern = Pattern.compile(columnDatatypeRegex);
        Matcher columnDatatypeMatcher;
        String columnDatatypeMatch = "";

        /**
        * Regex for fetching columnSize.
        * Using only columns Regex output.
        */

        String columnSizeRegex = "(\"column_size\"([\\s]*):([\\s]*)\")(.*?)(\",){1}";
        Pattern columnSizePattern = Pattern.compile(columnSizeRegex);
        Matcher columnSizeMatcher;
        String columnSizeMatch = "";

        /**
        * Regex for fetching columnsConstraints.
        * Using only columns Regex output.
        */

        String columnConstraintsRegex = "(\"constraints\"([\\s]*):([\\s]*)['{'])([\\s\\S]*?)(?=})";
        Pattern columnConstraintsPattern = Pattern.compile(columnConstraintsRegex);
        Matcher columnConstraintsMatcher;
        String columnConstraintsMatch = "";

        /**
        * Regex for fetching singl constraint.
        * Using only columnConstraints Regex output.
        */

        String constraintsRegex = "(\"[\\S]*?\"([\\s]*):([\\s]*)\")(.*?)(\")";
        Pattern constraintsPattern = Pattern.compile(constraintsRegex);
        Matcher constraintsMatcher;
        String constraintsMatch = "";

        //Will store details of all columns.
        String columnsDetails;

        //Will store columnName.
        String columnName = "";

        //Will store columnDatatype.
        String columnDatatype = "";

        //Will store columnSize.
        String columnSize = "";

        //Will store all constraints of a single column.
        String columnConstraints = "";

        //Will store only the constaint's data that has to be added in the sql query of single column.
        String constraints = "";

        //Will store entire column details includes columnName, its datatype, its size and all its constraints.
        String columnDetails = "";

        //Will store all the columnsNames which will together be primary keys.
        String primaryKeys = "";

        //Will store the columnNames which refers to some column.
        String foreignKey = "";
        String referedTableName = "";
        String referedColumnName = "";        

        /**
        * This while loop will get executed for each columns entry(all as one object) in json file.
        */
        if(columnsMatcher.find()){
            //Fetching all columns.
            columnsDetails = columnsMatcher.group(4);

            columnNameMatcher = columnNamePattern.matcher(columnsDetails);
            columnNameMatcher.reset();

            columnDatatypeMatcher = columnDatatypePattern.matcher(columnsDetails);
            columnDatatypeMatcher.reset();

            columnSizeMatcher = columnSizePattern.matcher(columnsDetails);
            columnSizeMatcher.reset();

            columnConstraintsMatcher = columnConstraintsPattern.matcher(columnsDetails);
            columnConstraintsMatcher.reset();

            columnDetails = "";    
            String comma = ", ";
            
            /**
            * This while loop will get executed for each column entry to fetch its name, datatype, size and constraints(all as one object) in json file.
            */
            
            primaryKeys = "";
            foreignKey = "";

            while(columnNameMatcher.find() && columnDatatypeMatcher.find() && columnSizeMatcher.find() && columnConstraintsMatcher.find()){
                //Fetching columnName.
                columnName = "`" + columnNameMatcher.group(4).toLowerCase() + "`";  
                
                //Fetching columnDatatype.
                columnDatatype = columnDatatypeMatcher.group(4);

                if(columnDatatype.equalsIgnoreCase("timestamp"))
                    columnDatatype += " NULL";

                /**
                * JSON file stores String as datatype for varchar!
                */
                if(columnDatatype.equalsIgnoreCase("string"))
                    columnDatatype = "varchar";

                //Fetching columnSize.
                columnSize = columnSizeMatcher.group(4);
                
                if(columnSize.equals(""))
                    //Storing columnDetails including columnName, its datatype as there is no size mention for the given column.
                    columnDetails += columnName + " " + columnDatatype;
                else
                    //Storing columnDetails including columnName, its datatype and size.
                    columnDetails += columnName + " " + columnDatatype + "(" + columnSize + ")";

                //Fetching all constraints of column.
                columnConstraints = columnConstraintsMatcher.group(4);
    
                constraintsMatcher = constraintsPattern.matcher(columnConstraints);
                constraintsMatcher.reset();

                constraints = "";

                /**
                * This while loop will get executed for each constraint entry in json file.
                */ 
                while(constraintsMatcher.find()){
                
                    //For DEFAULT, we need to get the value from JSON file as well include 'DEFAULT' keyword in the sql query.
                    if(constraintsMatcher.group(1).contains("default")){
                        if(constraintsMatcher.group(4).equals("CURRENT_TIMESTAMP") || constraintsMatcher.group(4).equals("NULL"))
                            constraints += " DEFAULT " + constraintsMatcher.group(4);
                        else
                            constraints += " DEFAULT '" + constraintsMatcher.group(4) + "'";
                    }
                        
                    
                    //For CHECK, we need to get the value from JSON file as well include 'CHECK' keyword in the sql query.
                    else if(constraintsMatcher.group(1).contains("check"))
                        constraints += " CHECK " + constraintsMatcher.group(4);

                    //For Primary Key, we need to get all the values from JSON file for current table which together creates composite primary key in the sql query.
                    else if(constraintsMatcher.group(1).contains("primary_key"))
                        primaryKeys += columnName + ", ";

                    //For Foreign Key, we need to get foreign_key from JSON file for current columnto be included in the sql query.
                    else if(constraintsMatcher.group(1).contains("foreign_key")){
                        referedTableName = (constraintsMatcher.group(4).substring(0, constraintsMatcher.group(4).indexOf('(')));
                        referedColumnName = "`" + (constraintsMatcher.group(4).substring(constraintsMatcher.group(4).indexOf('(')+1, constraintsMatcher.group(4).indexOf(')'))) + "`";
                        foreignKey += ", FOREIGN KEY (" + columnName + ") REFERENCES " + referedTableName + "(" + referedColumnName + ")";            
                    }

                    //For Remaining Constraints (PRIMARY KEY, AUTO_INCREMENT, UNIQUE and NOT NULL), we only need to get the value from JSON file to include in the sql query.)
                    else
                        constraints +=  " " + constraintsMatcher.group(4);
                }

                //Stores the entire detail for a column.
                columnDetails += constraints + comma;
            }

            //Removing the "," (comma) from the last primary key's detail.
            if(primaryKeys.length() != 0)
                primaryKeys = primaryKeys.substring(0, primaryKeys.length()-2);

            //Removing the " ," (String comma) from the last column's details.
            columnDetails = columnDetails.substring(0, columnDetails.length()-2);

            //Appending Primary Keys into columnDetails
            columnDetails += ", PRIMARY KEY (" + primaryKeys + ")";

            //Appending Foreign Keys into columnDetails
            columnDetails += foreignKey;

            //CREATE Query
            String sql = "CREATE TABLE IF NOT EXISTS " + tableName + " (" + columnDetails + ");";
            
            try{
                ps = conn.prepareStatement(sql);
                ps.executeUpdate();
            }catch(SQLException sqle){
                System.out.println("Some SQLException Occured : "  + sqle);
            }catch(Exception e){
                System.out.println("Exception Occured : " + e);
            }finally{
                try{
                    ps.close();
                }catch(Exception e){
                    System.out.println("Exception Occured In Finally : "  + e);
                }
            }
        }

        //No Error Occured
        return 1;
    }
}