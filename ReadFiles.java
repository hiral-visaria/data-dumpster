import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.FileNotFoundException;

class ReadFiles{
    /**
     * returns : String
     * The below written method takes String fileName for input 
     * And provides the data of that file.
     * Common for all operations which requires to read the file.
     * For Example : config.ini and tables.json file.
     */

    public static String readsFromGivenFile(String fileName){
        String readFile = "";
		File inFile = new File(fileName);
        FileInputStream ins = null;
        
        try{
            ins = new FileInputStream(inFile);
            int b;
            while((b = ins.read()) != -1)
                readFile += (char)b;

        }catch(FileNotFoundException fnfe){
            System.out.println("Some FileNotFoundException Occured : "  + fnfe);
        }catch(IOException ioe){
            System.out.println("Some IOException Occured : "  + ioe);
        }catch(Exception e){
            System.out.println("Exception Occured : " + e);
        }finally{
            try{
				ins.close();
			}catch(Exception e){
				System.out.println("Exception Occured In Finally : "  + e);
			}
        }

        //Returning the file as String.
        return readFile;
    }
}